exports.reply = function (...args) {
  return function (usr) {
    return usr.reply(...args);
  }
}

exports.scene = function (...args) {
  return function (usr) {
    return usr.scene(...args);
  }
}

exports.back = function (usr) {
  return usr.scene('../');
}