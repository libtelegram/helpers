const { Scene } = require('../libtelegram');

const { reply } = require('./utils');

const Menu = function (opts = { Text: '...' }, keyboard = []) {
  if (arguments.length !== 2) {
    throw new Error('Menu requires 2 arguments');
  }

  let text,
      resize_keyboard, 
      one_time_keyboard, 
      selective;

  if (typeof opts === 'string') {
    text = opts;
  }

  if (typeof opts === 'object') {
    text = opts.Text;
    resize_keyboard = opts.resize_keyboard;
    one_time_keyboard = opts.one_time_keyboard;
    selective = opts.selective;
  }

  if (!text) {
    throw Error('Menu requires text');
  }

  const menu = new Scene();

  for (let button of keyboard.flat()) {
    const handlers = normalizeHandlers(button, menu);

    menu.text(button[0], ...handlers);
  }

  const extra = Object.assign({
    reply_markup: JSON.stringify({
      keyboard: normalizeButtons(keyboard),
      resize_keyboard,
      one_time_keyboard,
      selective
    })
  }, typeof opts === 'object'?.opts);

  menu.enter(
    reply(text, extra)
  );

  return menu;
}

function normalizeButtons (buttons) {
  for (let i = buttons.length-1; i>=0; i--) {
    for (let j = buttons[i].length-1; j>=0; j--) {
      buttons[i][j] = { text: buttons[i][j][0] };
    }
  }

  return buttons;
}

function normalizeHandlers (button, menu) {
  const handlers = [];

  for (let i = button.length - 1; i > 0; i--) {
    if (typeof button[i] === 'function') {
      handlers.push(button[i]);

      continue;
    }

    menu.use(`path${i}`, button[i]);

    handlers.push(function (usr) {
      return usr.scene(button[i].mountpath);
    });
  }

  return handlers;
}

module.exports = Menu;